﻿namespace POC.Sophos.DependencyInyection.Service
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using POC.Sophos.Interfaces;
    using POC.Sophos.Service;

    public class CalculadoraInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<ICalculadora>().ImplementedBy<CalculadoraService>().Named("CalculadoraService").LifeStyle.PerWebRequest
                );
        }
    }
}
