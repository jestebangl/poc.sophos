﻿namespace POC.Sophos.DependencyInyection.Infraestructure
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using POC.Sophos.Infraestructure;
    using POC.Sophos.Interfaces;

    public class DatosInstaller : IWindsorInstaller
    {
        public string connectionString { get; set; }
        public DatosInstaller(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IDatos>().ImplementedBy<Datos>().Named("datos")
                    .DependsOn(Dependency.OnValue("ConnectionString", connectionString)).LifeStyle.PerWebRequest
                );
        }
    }
}
