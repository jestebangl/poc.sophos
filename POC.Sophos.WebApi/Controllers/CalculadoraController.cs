﻿namespace POC.Sophos.WebApi.Controllers
{
    using POC.Sophos.Interfaces;
    using System.Web.Http;


    public class CalculadoraController : ApiController
    {
        public ICalculadora CalculadoraService { get; set; }

        public CalculadoraController(ICalculadora CalculadoraService)
        {
            this.CalculadoraService = CalculadoraService;
        }

        [HttpGet]
        [Route("api/calculadora/sumar")]
        public IHttpActionResult Sumar(int a, int b)
        {
            return Ok(CalculadoraService.Sumar(a, b));
        }

    }
}
