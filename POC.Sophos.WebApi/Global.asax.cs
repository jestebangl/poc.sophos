namespace POC.Sophos.WebApi
{
    using Castle.MicroKernel.Lifestyle;
    using Castle.MicroKernel.Resolvers.SpecializedResolvers;
    using POC.Sophos.DependencyInyection.Infraestructure;
    using POC.Sophos.DependencyInyection.Service;
    using POC.Sophos.WebApi.Windsor;
    using System.Configuration;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;


    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializeIoC(GlobalConfiguration.Configuration);
        }

        public static void InitializeIoC(HttpConfiguration configuration)
        {
            var instalador = Ragolo.Core.IoC.IocHelper.Instance;
            var contenedor = instalador.GetContainer();

            instalador.Install(new DatosInstaller(ConfigurationManager.ConnectionStrings["SarlaftConnectionString"].ToString()));
            instalador.Install(new CalculadoraInstaller());
            instalador.Install(new Installer());

            WindsorControllerFactory controllerFactory = new WindsorControllerFactory(contenedor.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            contenedor.Kernel.Resolver.AddSubResolver(new CollectionResolver(contenedor.Kernel, true));
            contenedor.BeginScope();
            var dependencyResolver = new WindsorDependencyResolver(contenedor);
            configuration.DependencyResolver = dependencyResolver;
        }
    }
}
