﻿namespace POC.Sophos.Infraestructure
{
    using POC.Sophos.Interfaces;


    public class Datos : IDatos
    {
        private string ConnectionString { get; set; } 

        public Datos(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
        }

        public int Sumar(int a, int b)
        {
            return a + b;
        }
    }
}
