﻿namespace POC.Sophos.Service
{
    using POC.Sophos.Interfaces;
    public class CalculadoraService : ICalculadora
    {
        public IDatos datos { get; set; }

        public CalculadoraService(IDatos datos)
        {
            this.datos = datos;
        }

        public int Multiplicar(int a, int b)
        {
            return a + b;
        }

        public int Sumar(int a, int b)
        {
            return datos.Sumar(a, b);
        }
    }
}
