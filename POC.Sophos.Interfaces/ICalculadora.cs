﻿namespace POC.Sophos.Interfaces
{
    public interface ICalculadora
    {
        int Sumar(int a, int b);

        int Multiplicar(int a, int b);
    }
}
